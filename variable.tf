variable "vps-name" {
  type        = string
  description = "VPS name"
}

variable "vps-platform-id" {
  type        = string
  description = "VPS Platform ID"
}

variable "vps-zone" {
  type        = string
  description = "VPS Zone"
}

variable "vps-resource-core" {
  type        = number
  description = "VPS Resource core"
}

variable "vps-resource-memory" {
  type        = number
  description = "VPS Resource memory"
}

variable "vps-image-id" {
  type        = string
  description = "VPS Image id"
}

variable "vps-subnet-id" {
  type        = string
  description = "VPS Subnet id"
}

variable "vps-public-ip-bool" {
  type        = bool
  description = "VPS Public IP bool"
  default     = false
}

variable "vps-public-ip" {
  type        = string
  description = "VPS Public IP"
  nullable    = true
  default     = null
}

variable "vps-labels" {
  type        = map(any)
  description = "VPS Label stand"
  nullable    = true
  default     = null
}