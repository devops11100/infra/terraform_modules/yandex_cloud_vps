resource "yandex_compute_instance" "vps" {
  name = var.vps-name

  labels = {
    "created" = "terraform"
    "stand"   = var.vps-labels["stand"]
    "app"     = var.vps-labels["app"]
  }

  platform_id = var.vps-platform-id
  zone        = var.vps-zone

  resources {
    cores  = var.vps-resource-core
    memory = var.vps-resource-memory
  }

  boot_disk {
    initialize_params {
      image_id = var.vps-image-id
    }
  }

  network_interface {
    subnet_id      = var.vps-subnet-id
    nat            = var.vps-public-ip-bool
    nat_ip_address = var.vps-public-ip
  }

  metadata = {
    ssh-keys = "opusdv:${file("~/.ssh/id_rsa.pub")}"
  }
}